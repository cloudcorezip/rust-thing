table! {
    students (id) {
        id -> Int2,
        name -> Varchar,
        age -> Numeric,
        identity -> Varchar,
    }
}
