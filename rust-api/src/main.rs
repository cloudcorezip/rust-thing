#![feature(plugin)]
#![plugin(rocket_codegen)]

extern crate rocket;

#[macro_use] extern crate rocket_contrib;
#[macro_use] extern crate serde_derive;

use rocket_contrib::{Json, Value};

mod student;
use self::student::{Student};

#[post("/", data = "<student>" )]
fn create(student: Json<Student>) -> Json<Student>{
    student
}

#[get("/")]
fn read() -> Json<Value> {
    Json(json!([
        "student 1",
        "student 2"
    ]))
}

#[put("/<id>", data="<student>")]
fn update(id: i32, student: Json<Student>) -> Json<Student>{
    student
}

#[delete("/<id>")]
fn delete(id: i32) -> Json<Value>{
    Json(json!({"status" : "ok"}))

}

fn main() {
    rocket::ignite()
    .mount("/student", routes![create, update, delete])
    .mount("/students", routes![read])
    .launch();
}