### Installation

if you havent install Rust follow the instructions [HERE](https://doc.rust-lang.org/book/2018-edition/ch01-01-installation.html )

make sure use nightly version

```sh
$ rustup default nighltly
```

## Run

```sh
$ cargo run
```
