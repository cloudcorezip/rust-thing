create table students (
    id SMALLINT PRIMARY KEY ,
    name VARCHAR(60) NOT NULL,
    age numeric NOT NULL,
    identity VARCHAR(60) NOT NULL
)